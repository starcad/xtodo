﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XToDo.Service;

namespace XToDo
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Device.SetFlags(new[] {
                "SwipeView_Experimental"
            });

            MainPage = new NavigationPage(new MainPage()) { BarBackgroundColor = Color.FromHex("#3C9CAA"), BarTextColor = Color.White };
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private static DbService _dbService;
        public static DbService Db
        {
            get
            {
                if (_dbService == null)
                    _dbService = new DbService();

                return _dbService;
            }
            set => _dbService = value;
        }
    }
}
