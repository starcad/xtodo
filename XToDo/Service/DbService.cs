﻿using System.Collections.Generic;
using System.Linq;
using Realms;
using Xamarin.Forms;
using XToDo.Models;

namespace XToDo.Service
{
    /// <summary>
    /// Класс сервиса по работе с базой данных Realm
    /// </summary>
    public class DbService
    {
        private Realm realm;

        public DbService()
        {
            realm = Realm.GetInstance();
        }

        /// <summary>
        /// Метод получает все записи типа TodoItemModel и сортирует по дате
        /// </summary>
        public IEnumerable<TodoItemModel> GetAllTodos()
        {
            return realm.All<TodoItemModel>().OrderBy(t => t.Time);
        }

        /// <summary>
        /// Добавляет в базу новую задачу (объект типа TodoItemModel)
        /// </summary>
        /// <param name="todo">Задача (объект типа TodoItemModel)</param>
        public void AddTodo(TodoItemModel todo)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                realm.Write(() =>
                {
                    realm.Add(todo);
                });
            });
            
        }

        /// <summary>
        /// Удаляет все задачи из базы
        /// </summary>
        public void ClearTodos()
        {
            realm.Write(() =>
            {
                realm.RemoveAll();
            });
        }

        /// <summary>
        /// Обновляет ранее созданную задачу новыми параметрами
        /// </summary>
        /// <param name="todoItem">Выбранная из списка ранее созданная задача (объект типа TodoItemModel)</param>
        /// <param name="__title">Заголовок задачи</param>
        /// <param name="__details">Описание задачи</param>
        /// <param name="__datetime">Дата и время в long</param>
        public void SaveTodo(TodoItemModel task, string __title, string __details, long __datetime, int __state)
        {
            realm.Write(() =>
            {
                task.Title = __title;
                task.Details = __details;
                task.Time = __datetime;
                task.State = __state;
            });
        }

        public void ChangeTaskState(TodoItemModel task, int state)
        {
            realm.Write(() =>
            {
                task.State = state;
            });
        }

        /// <summary>
        /// Удаляет из базы ранее созданную задачу
        /// </summary>
        /// <param name="todo">Ранее созданная задача</param>
        public void DeleteTodo(TodoItemModel todo)
        {
            realm.Write(() =>
            {
                realm.Remove(todo);
            });
        }
    }
}