﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Realms;

namespace XToDo.Models
{
    public class TodoItemModel : RealmObject
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public long Time { get; set; }
        public int State { get; set; }


        public const int STATE_DEFAULT = 0;
        public const int STATE_INPROGRESS = 1;
        public const int STATE_DONE = 2;

        public static Dictionary<int, string> StateStr = new Dictionary<int, string>
        {
            { STATE_DEFAULT, ""},
            { STATE_INPROGRESS, "В процессе"},
            { STATE_DONE, "Готово"}
        };

        public static Dictionary<int, Color> StateColor = new Dictionary<int, Color>
        {
            { STATE_DEFAULT, Color.Transparent},
            { STATE_INPROGRESS, Color.YellowGreen},
            { STATE_DONE, Color.Green}
        };
    }
}
