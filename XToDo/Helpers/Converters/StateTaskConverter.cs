﻿using System;
using System.Globalization;
using Xamarin.Forms;
using XToDo.Models;

namespace XToDo.Helpers.Converters
{
    /// <summary>
    /// Конвертер управляет состоянием задачи. 
    /// </summary>
    class StateTaskConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int state = (int)value;
            string result = "";

            if(TodoItemModel.StateStr.ContainsKey(state))
            {
                result = TodoItemModel.StateStr[state];
            }

            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0;
        }
    }
}
