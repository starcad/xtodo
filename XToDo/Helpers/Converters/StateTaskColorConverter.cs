﻿using System;
using System.Globalization;
using Xamarin.Forms;
using XToDo.Models;

namespace XToDo.Helpers.Converters
{
    /// <summary>
    /// Конвертер управляет цветом состояния задачи. 
    /// </summary>
    class StateTaskColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int state = (int)value;
            Color result = Color.Transparent;

            if(TodoItemModel.StateColor.ContainsKey(state))
            {
                result = TodoItemModel.StateColor[state];
            }

            return result;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0;
        }
    }
}
