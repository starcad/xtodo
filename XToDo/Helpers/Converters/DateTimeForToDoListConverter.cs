﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace XToDo.Helpers.Converters
{
    /// <summary>
    /// Конвертер управляет отображением даты. 
    /// На вход получает дату и время в виде long Time Ticks - на выходе форматированную строку
    /// </summary>
    class DateTimeForToDoListConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            long timeTicks = (long)value;

            return new DateTime(timeTicks).ToString("dd.MM.yyyy HH:mm");
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 0;
        }
    }
}
