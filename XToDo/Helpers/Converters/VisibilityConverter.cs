﻿using System;
using Xamarin.Forms;


namespace XToDo.Helpers.Converters
{
    /// <summary>
    /// Конвертер управления видимостью элементов XAML. Параметром управляется направление прямое и инвертированное.
    /// </summary>
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string __typeOfConverting = parameter?.ToString() ?? "Normal";

            bool result = true;

            if(value == null)
            {
                result = false;
            }
            else if(value.GetType() == typeof(string))
            {
                result = !string.IsNullOrEmpty(value as string);
            }
            else if(value.GetType() == typeof(bool))
            {
                result = (bool)value;
            }

            return __typeOfConverting == "Invert" ? !result : result;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
        System.Globalization.CultureInfo culture)
        {
            //We can't support this
            throw new NotImplementedException();
        }
    }
}
