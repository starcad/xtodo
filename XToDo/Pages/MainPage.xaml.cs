﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using XToDo.Models;
using XToDo.Pages;

namespace XToDo
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private BindingPageDataWrapperClass __bindingObj = new BindingPageDataWrapperClass();

        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            var list = App.Db.GetAllTodos();

            __bindingObj.IsVisible = list.Count() > 0;
            __bindingObj.listOfTask = list;

            BindingContext = __bindingObj;
        }


        private async void DeleteAllClicked_Handler(object sender, EventArgs e)
        {
            bool result = await DisplayAlert("Внимание!", "Очистить ввесь список задач?", "Да", "Нет");

            if(result)
            {
                App.Db.ClearTodos();
                __bindingObj.IsVisible = false;
            }
        }

        private async void AddToDo_ClickedHandler(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new EditToDoPage(new TodoItemModel()));
        }

        private async void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            TodoItemModel selectedTask = (TodoItemModel)e.Item;

            await Navigation.PushAsync(new EditToDoPage(selectedTask));
        }

        private void SwipeTaskUnDone_Invoked(object sender, EventArgs e)
        {
            var item = sender as SwipeItem;
            var model = item.BindingContext as TodoItemModel;

            App.Db.ChangeTaskState(model, TodoItemModel.STATE_DEFAULT);
        }

        private void SwipeTaskDone_Invoked(object sender, EventArgs e)
        {
            var item = sender as SwipeItem;
            var model = item.BindingContext as TodoItemModel;

            App.Db.ChangeTaskState(model, TodoItemModel.STATE_DONE);
        }

        private void SwipeTaskInProgress_Invoked(object sender, EventArgs e)
        {
            var item = sender as SwipeItem;
            var model = item.BindingContext as TodoItemModel;

            App.Db.ChangeTaskState(model, TodoItemModel.STATE_INPROGRESS);
        }

        /// <summary>
        /// Класс-обертка для привязки данных на страницу
        /// Потенциальная модель привязки при использовании MVVM паттерна :)
        /// </summary>
        private class BindingPageDataWrapperClass : INotifyPropertyChanged
        {
            private IEnumerable _list;
            private bool isvis;
            public IEnumerable listOfTask
            {
                get
                {
                    return _list;
                }
                set
                {
                    if (_list != value)
                    {
                        _list = value;
                        OnPropertyChanged("listOfTask");
                    }
                }
            }
            public bool IsVisible
            {
                get
                {
                    return isvis;
                }
                set
                {
                    if (isvis != value)
                    {
                        isvis = value;
                        OnPropertyChanged("IsVisible");
                    }
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;
            public void OnPropertyChanged(string prop = "")
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
    }
}
