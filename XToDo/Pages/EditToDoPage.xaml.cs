﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XToDo.Models;

namespace XToDo.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditToDoPage : ContentPage
    {
        private DateTime dDate { get; set; }
        private TimeSpan tTime { get; set; }
        private DateTime oldDateTime { get; set; } = DateTime.Now;
        private DateTime resultDateTime { get; set; } = DateTime.Now;
        private TodoItemModel editingTodoItem { get; set; }

        private bool isTimeWasChanged = false;

        public EditToDoPage(TodoItemModel todoItem)
        {
            InitializeComponent();

            BindingPageDataWrapperClass bindingData = new BindingPageDataWrapperClass();

            editingTodoItem = todoItem;

            List<string> states = TodoItemModel.StateStr.Values.ToList();
            states[0] = "Нет";
            bindingData.states = states;

            if (editingTodoItem.Id != null)
            {
                oldDateTime = new DateTime(editingTodoItem.Time);

                bindingData.pageTitle = "Редактировать";
                bindingData.taskTitle = editingTodoItem.Title;
                bindingData.taskDetails = editingTodoItem.Details;
                bindingData.dDate = oldDateTime;
                bindingData.tTime = oldDateTime.TimeOfDay;
            }
            else
            { 
                bindingData.pageTitle = "Новая задача";
            }

            BindingContext = bindingData;

            StatePicker.SelectedIndex = editingTodoItem.State;
        }

        private void datePicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            dDate = DatePkr.Date;

            resultDateTime = isTimeWasChanged ? dDate.AddMinutes(tTime.Minutes).AddHours(tTime.Hours)
               : dDate.AddMinutes(oldDateTime.TimeOfDay.Minutes).AddHours(oldDateTime.TimeOfDay.Hours);
        }

        private void TimePicker_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Time")
            {
                TimePicker tm = (TimePicker)sender;

                if(tm.Time.Hours != oldDateTime.TimeOfDay.Hours  ||  tm.Time.Minutes != oldDateTime.TimeOfDay.Minutes)
                {
                    isTimeWasChanged = true;

                    tTime = tm.Time;

                    resultDateTime = dDate.AddHours(tm.Time.Hours).AddMinutes(tm.Time.Minutes);
                }
            } 
        }

        private async void SaveToDo(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(TitleField.Text))
            {
                if(editingTodoItem.Id != null)
                {
                    App.Db.SaveTodo(editingTodoItem, TitleField.Text, DetailsField.Text, resultDateTime.Ticks, StatePicker.SelectedIndex);
                }
                else
                {
                    editingTodoItem.Id = Guid.NewGuid().ToString();
                    editingTodoItem.Title = TitleField.Text;
                    editingTodoItem.Details = DetailsField.Text;
                    editingTodoItem.Time = resultDateTime.Ticks;
                    editingTodoItem.State = StatePicker.SelectedIndex;

                    App.Db.AddTodo(editingTodoItem);
                }

                await Navigation.PopAsync();
            }            
        }

        private async void DeleteToDo(object sender, EventArgs e)
        {
            if (editingTodoItem.Id != null)
            {
                App.Db.DeleteTodo(editingTodoItem);

            }

            await Navigation.PopAsync();
        }

        /// <summary>
        /// Класс-обертка для привязки данных на форму
        /// </summary>
        private class BindingPageDataWrapperClass
        {
            public DateTime dDate { get; set; } = DateTime.Now;
            public TimeSpan tTime { get; set; } = DateTime.Now.TimeOfDay;

            public string taskTitle { get; set; }
            public string taskDetails { get; set; }
            public string pageTitle { get; set; }
            public List<string> states { get; set; }
        }
    }
}